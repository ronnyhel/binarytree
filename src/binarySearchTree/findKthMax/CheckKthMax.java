package binarySearchTree.findKthMax;

class CheckKthMax {

	/**
	 * Given the root to a binary search tree--a number "k"--write a function
	 * tofind the kth maximum value in that tree.
	 * 
	 * @param root
	 * @param k
	 * @return the max kth value of the bst
	 */
	public static int findKthMax(Node root, int k) {

		// Write - Your - Code
		return Integer.MAX_VALUE;
	}
	
	public static void main(String args[]) {

	    BinarySearchTree bsT = new BinarySearchTree();

	    bsT.add(6);

	    bsT.add(4);
	    bsT.add(9);
	    bsT.add(5);
	    bsT.add(2);
	    bsT.add(8);

	    System.out.println(findKthMax(bsT.getRoot(),3));
	  }
}