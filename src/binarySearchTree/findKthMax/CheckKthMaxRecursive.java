package binarySearchTree.findKthMax;

class CheckKthMaxRecursive {
	
	private static int counter;
	
	

	/**
	 * Given the root to a binary search tree--a number "k"--write a function to
	 * find the kth maximum value in that tree. A solution is placed in the
	 * "solution" section to help you, but we would suggest you try to solve it on
	 * your own first.
	 * 
	 * @param root
	 * @param k
	 * @return the max kth value of the bst
	 */
	public static int findKthMax(Node root, int k) {

		counter = 0;
		Node maxKNode = findKthMaxRecursive(root,k);
		if(maxKNode!=null)
			return maxKNode.getData();
		else
			return Integer.MAX_VALUE;
		
		
	}



	private static Node findKthMaxRecursive(Node root, int k) {
		if(root == null)
			return null;
				
		Node node = findKthMaxRecursive(root.rightChild, k);
		
		if(k != counter) {
			counter++;
			node = root;
		}
		
		if(k==counter) {
			return node;
		}else {
			return findKthMaxRecursive(root.leftChild, k);
		}		
			
	}
	
	public static void main(String args[]) {

	    BinarySearchTree bsT = new BinarySearchTree();

	    bsT.add(6);

	    bsT.add(4);
	    bsT.add(9);
	    bsT.add(5);
	    bsT.add(2);
	    bsT.add(8);

	    System.out.println(findKthMax(bsT.getRoot(),3));
	  }
}