package binarySearchTree.findKthMax;

class CheckKthMaxIterative {

	/**
	 * Given the root to a binary search tree--a number "k"--write a function
	 * tofind the kth maximum value in that tree.
	 * 
	 * @param root
	 * @param k
	 * @return the max kth value of the bst
	 */
	public static int findKthMax(Node root, int k) {

		StringBuilder sb = new StringBuilder();
		inOrderTraversal(root, sb);
		System.out.println(sb.toString());

		String[] valArr = sb.toString().split(",");
		if (valArr.length - k >= 0)
			return Integer.parseInt(valArr[valArr.length - k]);
		else
			return Integer.MAX_VALUE;
	}

	private static void inOrderTraversal(Node root, StringBuilder sb) {

		if (root.leftChild != null)
			inOrderTraversal(root.leftChild, sb);
		sb.append(root.getData()).append(",");
		if (root.rightChild != null)
			inOrderTraversal(root.rightChild, sb);

	}

	public static void main(String args[]) {

		BinarySearchTree bsT = new BinarySearchTree();

		bsT.add(6);

		bsT.add(4);
		bsT.add(9);
		bsT.add(5);
		bsT.add(2);
		bsT.add(8);

		System.out.println(findKthMax(bsT.getRoot(), 3));
	}
}
